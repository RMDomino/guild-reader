import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlayerModel {

    private String id;
    private String name;
    private int allyCode;
    @JsonProperty("gp")
    private int totalGp;
    @JsonProperty("gpChar")
    private int infantryGp;
    @JsonProperty("gpShip")
    private int fleetGp;
    @JsonProperty("guildRefId")
    private String guildId;
    @JsonProperty("guildMemberLevel")
    private int level;
}
