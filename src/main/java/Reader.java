import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import help.swgoh.api.SwgohAPI;
import help.swgoh.api.SwgohAPIBuilder;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class Reader {


    private void init() throws ExecutionException, InterruptedException, JsonProcessingException {
        SwgohAPI swgohAPI = new SwgohAPIBuilder().withUsername("YOUR_USERNAME").withPassword("YOUR_PASSWORD").build();
        String guild = swgohAPI.getGuild(388937783).get();
        ObjectMapper mapper = new ObjectMapper();
        GuildModel guildModel = mapper.readValue(guild, new TypeReference<List<GuildModel>>(){})
                .stream()
                .findAny()
                .orElseThrow();
        guildModel
                .getRoster()
                .forEach(playerModel -> System.out.println(playerModel.getName() + " - " + playerModel.getAllyCode()));
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException, JsonProcessingException {
        new Reader().init();
    }
}
